using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    public class SqlContext : DbContext
    {
        public SqlContext(DbContextOptions<SqlContext> options) : base(options)
        {

        }

        public DbSet<Vendor> Vendors{ get; set; }
        public DbSet<Sale> Sales{ get; set; }
        public DbSet<Item> Items{ get; set; }
    }
}