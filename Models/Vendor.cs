namespace tech_test_payment_api.Models
{
    public class Vendor
    {
        public int Id { get; set; }
        public bool Active { get; set; }
        public string Cpf { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
    }
}