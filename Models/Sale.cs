namespace tech_test_payment_api.Models
{
    public class Sale
    {
        public int Id { get; set; }
        public int IdVendor { get; set; }
        public string Protocol { get; set; }
        public List<Item> Items { get; set; }
        public DateTime DateSale { get; set; }
        public string Status { get; set; }
    }
}