namespace tech_test_payment_api.Models
{
    public class Item
    {
        public int Id { get; set; }
        public string SaleProtocol { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Amount { get; set; }
    }
}