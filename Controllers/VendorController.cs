using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendorController : ControllerBase
    {
        private readonly SqlContext _context;        
        public VendorController(SqlContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Create(Vendor vendor)
        {
            _context.Add(vendor);
            _context.SaveChanges();
            return Ok(vendor);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var vendorDataBase = _context.Vendors.Find(id);

            if(vendorDataBase == null)
                return NotFound("VENDEDOR NÃO ENCONTRADO!");            
            
            return Ok(vendorDataBase);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Vendor vendor)
        {
            var vendorBanco = _context.Vendors.Find(id);

            if(vendorBanco == null)
                return NotFound("VENDEDOR NÃO ENCONTRADO!");

            vendorBanco.Name = vendor.Name;
            vendorBanco.Cpf = vendor.Cpf;
            vendorBanco.Active = vendor.Active;
            vendorBanco.Email = vendor.Email;
            vendorBanco.Telephone = vendor.Telephone;

            _context.Vendors.Update(vendorBanco);
            _context.SaveChanges();

            return Ok(vendorBanco);
        }
    }
}