using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SaleController : ControllerBase
    {
        private readonly SqlContext _context;
        public SaleController(SqlContext context)
        {
            _context = context;
        }        

        [HttpPost]
        public IActionResult Create(Sale sale)
        {
            sale.DateSale = DateTime.Now;
            sale.Protocol = sale.DateSale.ToString("yyyyMMddHHmmss");
            sale.Status = "Aguardando Pagamento";
            var vendorDataBase = _context.Vendors.Find(sale.IdVendor);

            // VERIFICA SE O VENDEDOR EXISTE PROCURANDO NO BD PELO ID FORNECIDO
            if (vendorDataBase == null)
                return BadRequest("VENDEDOR INEXISTENTE, POR FAVOR, SELECIONE UM VENDEDOR VÁLIDO OU CADASTRE UM NOVO!");

            if (!vendorDataBase.Active)
            {
                return BadRequest("O CADASTRO DO VENDEDOR ESTÁ INATIVO!");
            }

            // VERIFICA SE O STATUS INFORMADO É VÁLIDO
            if (sale.Status != "Aguardando Pagamento")
                return BadRequest("O STATUS INFORMADO É INVÁLIDO! A venda só pode ser criada com Status \"Aguardando Pagamento\"");

            // VERIFICAÇÃO DOS CAMPOS VAZIOS ANTES DE SALVAR A VENDA NO BANCO
            if (sale.Items == null)
            {
                return BadRequest("É PRECISO HAVER UM ITEM PARA CADASTRAR A VENDA!");
            }
            else
            {
                foreach (var item in sale.Items)
                {
                    if (item.Name == null || item.Name == "")
                    {
                        return BadRequest("O NOME DO ITEM NÃO PODE ESTAR VAZIO!");
                    }
                    else if (item.Amount == 0)
                    {
                        return BadRequest("A QUANTIDADE DE ITENS NÃO PODER SER IGUAL A ZERO!");
                    }
                    else if (item.Price == 0)
                    {
                        return BadRequest("O VALOR DO ITEM NÃO PODER SER IGUAL A ZERO!");
                    }
                    else
                    {
                        item.SaleProtocol = sale.Protocol;
                    }
                }                
            }

            _context.Add(sale);
            _context.SaveChanges();
            return Ok(sale);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var saleDataBase = _context.Sales.Find(id);


            if(saleDataBase == null)
                return NotFound("VENDA NÃO ENCONTRADA!");
            
            // CRIA A LISTA DOS ITEMS CADASTRADOS USANDO O PROTOCOLO GERADO NA VENDA
            saleDataBase.Items = _context.Items.Where(item => item.SaleProtocol == saleDataBase.Protocol).ToList();

            var vendorDataBase = _context.Vendors.Find(saleDataBase.IdVendor);
            vendorDataBase.Cpf = "";
            List<Object> sale = new List<Object>();
            sale.Add(saleDataBase);
            sale.Add(vendorDataBase);
            return Ok(sale);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Sale sale)
        {
            var saleBanco = _context.Sales.Find(id);

            if(saleBanco == null)
                return NotFound("VENDA NÃO ENCONTRADA!");

            // VERIFICA SE O STATUS INFORMADO É VÁLIDO ANTES DE ATUALIZAR
            if (saleBanco.Status == "Aguardando Pagamento" && sale.Status == "Pagamento Aprovado" || sale.Status == "Cancelada")
            {
                saleBanco.Status = sale.Status;
            }
            else if(saleBanco.Status == "Pagamento Aprovado" && sale.Status == "Enviado para Transportadora" || sale.Status == "Cancelada")
            {
                saleBanco.Status = sale.Status;
            }
            else if(saleBanco.Status == "Enviado para Transportadora" && sale.Status == "Entregue")
            {
                saleBanco.Status = sale.Status;
            }
            else
            {
                return BadRequest("O STATUS INFORMADO É INVÁLIDO! O Status \"Aguardando Pagamento\" só pode ser alterado para \"Pagamento Aprovado\" ou \"Cancelada\", \"Pagamento Aprovado\" só pode ser alterado para \"Enviado para Transportadora\" ou \"Cancelada\", \"Enviado para Transportadora\" só pode ser alterado para \"Entregue\"");
            }

            _context.Sales.Update(saleBanco);
            _context.SaveChanges();

            return Ok(saleBanco);
        }
    }
}