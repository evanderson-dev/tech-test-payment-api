## UTILIZAÇÃO DA API
Uma venda só será realizada passando pelos seguintes requisitos obrigatórios:
1- É nescessário informar o Id de um vendedor cadastrado e Ativo, ao tentar cadastrar a venda será feita a consulta no BD usando o Id do vendedor, assim não será nescessário informar todos os dados do vendedor em cada venda.
2- A venda precisa haver pelo menos um item e que tenha nome, valor e quantidade.

Alguns dados da venda serão gerados Automaticamente:
1- Id da Venda.
2- Id de Cada Item.
3- Protocolo, que também sera adicionado a cada item.
4- Data e hora da venda.
5- O Status da venda sempre será salvo como "Aguardando Pagamento", assim não é obrigatorio informá-lo.

## DADOS AUTOMATICOS E OBRIGATORIOS PARA REALIZAR A VENDA
{
  "id": 0, ------------------------- Automatico
  "idVendor": 0, ------------------- Obrigatorio
  "protocol": "string", ------------ Automatico
  "items": [
    {
      "id": 0, --------------------- Automatico
      "saleProtocol": "string", ---- Automatico
      "name": "string", ------------ Obrigatorio
      "price": 0, ------------------ Obrigatorio
      "amount": 0 ------------------ Obrigatorio
    }
  ],
  "dateSale": "2022-12-06 02:23", -- Automatico
  "status": "string" --------------- Automatico
}

## DADOS AUTOMATICOS E OBRIGATORIOS PARA CADASTRAR O VENDEDOR
{
  "id": 0, ------------------------- Automatico
  "active": true, ------------------ Obrigatorio
  "cpf": "string", ----------------- Obrigatorio
  "name": "string", ---------------- Obrigatorio
  "email": "string", --------------- Obrigatorio
  "telephone": "string" ------------ Obrigatorio
}

## CONSULTA DA VENDA REALIZADA
Pode ser feito a Busca da venda pelo Id da venda que buscará todos os dados da venda, dos items e do vendedor, não exibindo o Cpf do vendedor.

## UPDATE DA VENDA
A atualização de status permite somente as seguintes transições:

De: Aguardando pagamento Para: Pagamento Aprovado
De: Aguardando pagamento Para: Cancelada
De: Pagamento Aprovado Para: Enviado para Transportadora
De: Pagamento Aprovado Para: Cancelada
De: Enviado para Transportador. Para: Entregue

## CRIAÇÃO DO BANCO DE DADOS
Comandos para instalação dos Pacotes Nescessários para o funcionamento da API:
dotnet tool install --global dotnet-ef --version 7.0.0
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package Microsoft.EntityFrameworkCore.Design

A pasta Migrations já está criada com os arquivos nescessários, basta executar o seguinte comando no Terminal para a criação do BD:
dotnet-ef database update